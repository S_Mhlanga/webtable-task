# WebTable Task

Automation Framework for Web UI testing.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

Java installed and system variables set

Maven installed system variable set

Eclipse IDE or similar to review code


## Running the tests
Open cmd from project folder.
Run following commands.

```
mvn eclipse:eclipse
mvn compile
mvn test

```
Alternatively ,run testng.xml from Eclipse IDE.

NB-Extents reports get generated when running test with maven

### Reporting

Reports are located in tartget folder

WebTable-Task\target\surefire-reports\ExtentReportsTestNG.html

### Coding style 

Standard

## Built With

* [Selenium with Java]
* [TestNG] 
* [Maven]

