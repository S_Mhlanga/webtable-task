package cib.digital.tests;

import java.io.IOException;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import cib.digital.pages.AddUserPopUp;
import cib.digital.pages.UsersTablePage;
import cib.digital.resources.TestBase;



public class AddUserTest extends TestBase {
	
	public static Logger log =LogManager.getLogger(TestBase.class.getName());
	//Status Logger

	@BeforeTest
	
	public void initialize() throws IOException
	{
		driver=initializeDriver();
		log.info("Driver is initialized");
		
		driver.get("http://www.way2automation.com/angularjs-protractor/webtables/");
		log.info("Navigated to Users Table");
		
		
	}
	
	
	@Test (priority=0)
	
	public void ValidateUsersTablePage() throws IOException
	{
		
		Assert.assertEquals(driver.getTitle(), "Protractor practice website - WebTables");
		log.info("Verified navigation to correct web page");
		//Verifies that browser opened Users Table
			
	}
	
	@Test(dataProvider="getData", priority=1)

	public void AddUsers(String Firstname, String LastName, String Username, String Password, String Company, String Role, String Email, String Cellphone) throws IOException
	{
		
		
		UsersTablePage ut=new UsersTablePage(driver);
		ut.addUser().click();
		log.info("Add User button clicked");
		//Clicks Add User button

		AddUserPopUp ad=new AddUserPopUp(driver);
		
		
		ad.getFirstName().clear();
		ad.getLastName().clear();
		ad.getUserName().clear();
		ad.getPassword().clear();
		ad.getEmail().clear();
		ad.getCellphone().clear();
		
		//Clears input fields
		
		ad.getFirstName().sendKeys(Firstname);
		ad.getLastName().sendKeys(LastName);
		ad.getUserName().sendKeys(Username);
		ad.getPassword().sendKeys(Password);
		ad.getCustomer().findElement(By.xpath(Company)).click();
		ad.getRole().selectByVisibleText(Role);
		ad.getEmail().sendKeys(Email);
		ad.getCellphone().sendKeys(Cellphone);
		ad.getSave().click();
		
		//Enters user data from data provider
		
	}
	
	@DataProvider
	public Object[][] getData()  //Provides user data
	{
		Object[][] data=new Object[2][8];
		
		data[0][0]="FName1";
		data[0][1]="LName1";
		data[0][2]="User1";
		data[0][3]="Pass1";
		data[0][4]="//input[@value='15']";
		data[0][5]="Admin";
		data[0][6]="admin@mail.com";
		data[0][7]="082555";
		//Adds first user
				
		data[1][0]="FName2";
		data[1][1]="LName2";
		data[1][2]="User2";
		data[1][3]="Pass2";
		data[1][4]="//input[@value='16']";
		data[1][5]="Customer";
		data[1][6]="customer@mail.com";
		data[1][7]="082444";
		//Adds second user	
		
		return data;
	}
	
	@Test(priority=2)
	public void ValidateAddedUser() throws IOException
	{
		
		Assert.assertEquals(driver.findElement(By.xpath("//tbody//tr[1]//td[3]")).getText(), "User2");
		log.info("Verified if user ");
		//validates that User2 user is added to the users table
		
		Assert.assertEquals(driver.findElement(By.xpath("//tbody//tr[2]//td[3]")).getText(), "User1");
		log.info("Verified if user ");
		//validates that User1 user is added to the users table
			
	}
	
	
	
	@AfterTest
	public void teardown()
	{
		driver.close();
		driver=null;
	}
		



}