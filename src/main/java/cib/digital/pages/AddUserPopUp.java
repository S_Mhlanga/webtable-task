package cib.digital.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class AddUserPopUp {
	
	public WebDriver driver;
	
	//Locates and identifies fields to complete when adding a user to the Users Table
	
	By firstName=By.xpath("//input[@name='FirstName']");
	By lastName=By.xpath("//input[@name='LastName']");
	By userName=By.xpath("//input[@name='UserName']");
	By password=By.xpath("//input[@name='Password']");
	By companyAAA=By.xpath("//input[@value='15']");
	By companyBBB=By.xpath("//input[@value='16']");
	By role=By.xpath("//select[@name='RoleId']");
	By email=By.xpath("//input[@name='Email']");
	By cellphone=By.xpath("//input[@name='Mobilephone']");
	By save=By.xpath("//button[@class='btn btn-success']");
	
	
	public AddUserPopUp(WebDriver driver) {
		
		this.driver=driver;
	}
	
	//Methods for the Web Elements and their respective fields
	public WebElement getFirstName()
	{
		return driver.findElement(firstName);
	}

	public WebElement getLastName()
	{
		return driver.findElement(lastName);
	}
	
	public WebElement getUserName()
	{
		return driver.findElement(userName);
	}
	
	public WebElement getPassword()
	{
		return driver.findElement(password);
	}
	
	public WebElement getCustomer()
	{
		WebElement rbCompany=driver.findElement(By.xpath("//input[@value='15']"));
		return rbCompany;
	}
	
	public WebElement getCompanyBBB()
	{
		return driver.findElement(companyBBB);
	}
	
	public WebElement getEmail()
	{
		return driver.findElement(email);
	}
	
	public WebElement getCellphone()
	{
		return driver.findElement(cellphone);
	}
	
	public Select getRole()
	{
		Select drpRole = new Select(driver.findElement(By.name("RoleId")));
		return drpRole;
			
	}
	
	public WebElement getSave()
	{
		return driver.findElement(save);
	}
}
