package cib.digital.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class UsersTablePage {
	
	public WebDriver driver;
	
	By addUserButton=By.xpath("//button[@type='add']");
	
	public UsersTablePage(WebDriver driver) {
		
		this.driver=driver;
	}

	public WebElement addUser()
	{
		return driver.findElement(addUserButton);
	}
	
	

}