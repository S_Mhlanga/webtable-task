package cib.digital.resources;



import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.google.common.io.Files;


public class TestBase {
	
	public static WebDriver driver;
	public Properties prop;
	

public WebDriver initializeDriver() throws IOException
{
	Properties prop=new Properties();
	FileInputStream fis=new FileInputStream("src\\main\\java\\cib\\digital\\resources\\data.properties");
	
	prop.load(fis);
	String browserName=prop.getProperty("browser");
	
	if(browserName.equals("Chrome"))
	{
		System.setProperty("webdriver.chrome.driver", "src\\main\\browserDrivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		//execute in Chrome browser
	}
	else if(browserName.equals("Firefox"))
	{
		System.setProperty("webdriver.firefox.marionette", "src\\main\\browserDrivers\\geckodriver.exe");
		driver=new FirefoxDriver();
		driver.manage().window().maximize();
		//execute in Firefox browser
	}
	else if (browserName.equals("Edge"))	
	{
		System.setProperty("webdriver.edge.driver", "src\\main\\browserDrivers\\MicrosoftWebDriver.exe");
		driver=new EdgeDriver();
		driver.manage().window().maximize();
		//execute in Microsoft Edge browser
	}
	
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	return driver;
}

	public void getScreenshot(String result) throws IOException 
	{
		File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		Files.copy(src, new File("src\\test\\Screenshots\\"+result+"screenshot.png"));
		
	}
}


